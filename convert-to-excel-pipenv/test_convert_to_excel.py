from convert_to_excel import parse_line


def test_parse_line():
    assert parse_line('0123456789', [0, 5]) == ['01234', '56789']
    assert parse_line('0123456789', [1]) == ['123456789']
    assert parse_line('0123456789', [0, 5, 20]) == ['01234', '56789', '']
