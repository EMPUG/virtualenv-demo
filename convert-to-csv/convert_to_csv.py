"""
Converts a fixed-length file to a delimited file. The input file, output
file and column numbers (1-based) are specified on the command line.

Example usage:

    python convert_to_csv.py input.txt output.txt 1 10 30
"""

import sys
import os


def print_usage():
    sys.stderr.write('Usage:\n')
    sys.stderr.write('    python {} COLUMN_1 [COLUMN_2 ...]\n\n'.format(sys.argv[0]))
    sys.stderr.write('Converts a fixed length file to a delimited file.\n')
    sys.stderr.write('Reads from standard input and writes to standard output.\n\n')


def parse_line(line, columns):
    """
    Parse a line into fields, given a list of column numbers (0-based).
    Return the list of fields.
    """
    fields = []
    column_count = len(columns)
    for n in range(column_count):
        start_col = columns[n]
        end_col = columns[n+1] if n < column_count - 1 else len(line)
        fields.append(line[start_col:end_col].strip())
    return fields


def main():
    # Make sure we have at least 3 command line arguments.
    if len(sys.argv) < 4:
        sys.stderr.write('Error: no column numbers were specified.\n\n')
        print_usage()
        sys.exit(1)

    # Display usage notes if the first command line argument is "-h" or "--help"
    if sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print_usage()
        sys.exit()

    # Get the name of the input and output files.
    input_fname = sys.argv[1]
    output_fname = sys.argv[2]

    # Make sure the input file exists.
    if not os.path.exists(input_fname):
        sys.stderr.write('Error: input file {} does not exist.\n\n' % input_fname)
        print_usage()
        sys.exit(1)

    # Convert column numbers to a 0-based list of integers.
    try:
        columns = list(map(lambda x: int(x)-1, sys.argv[3:]))
    except ValueError:
        sys.stderr.write('Error: Column numbers must be integers\n\n')
        print_usage()
        sys.exit(1)

    sys.stderr.write('Using column numbers {}\n'.format(columns))

    # Convert each line from the input file and write it to the output file.
    with open(input_fname) as input_file:
        with open(output_fname, 'w') as output_file:
            for line in input_file:
                fields = parse_line(line, columns)
                output_file.write('|'.join(fields) + '\n')


if __name__ == '__main__':
    main()
