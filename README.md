## Python Virtual Environments

This repository contains the materials used for a presentation about Python
virtual environments. The presentation discusses the traditional lower level
virtualenv approach and the newer `pipenv` utility.

This presentation is licensed under the Creative Commons Attribution 4.0
International license: https://creativecommons.org/licenses/by/4.0/

### docs

Contains the presentation slides.

### convert-to-csv

Contains `convert_to_csv.py`, a small program to convert a fixed-length file
to a pipe-delimited file. Since this program only uses the standard library,
no third-party packages are needed, and therefore a virtual environment is not
needed.

### convert-to-excel-venv

Contains `convert-to-excel.py`, a small program to convert a fixed-length file
to an Excel file, which requires the third-party `openpyxl` package.
This directory is used to demonstrate setting up a virtual environment with `venv`.

### convert-to-excel-pipenv

Contains `convert-to-excel.py`, a small program to convert a fixed-length file
to an Excel file, which requires the third-party `openpyxl` package.
This directory is used to demonstrate setting up a virtual environment with `pipenv`.
